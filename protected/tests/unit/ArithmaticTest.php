<?php

class ArithmaticTest extends CTestCase
{
	private $calc;
	
	function __construct() 
	{
		$this->calc = new Calc;
	}
	
	private function process($a,$b,$opt)
	{
		return $this->calc->process($a,$b,$opt);
	}
	
	public function testDiv()	
	{
		$this->assertEquals(5,$this->process(10,2,'/'));
	}
	
	public function testAdd()
	{
		$this->assertEquals(12,$this->process(10,2,'+'));
		
		$this->assertEquals(8,$this->process(10,2,'-'));
	}
	
	public function testSub()
	{
		$this->assertEquals(8,$this->process(10,2,'-'));
	}	
	
// 	public function testMul()
// 	{
// 		$this->assertEquals(20,$this->process(10,2,'*'));
// 	}
}