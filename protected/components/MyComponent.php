<?php

class MyComponent extends CComponent//CApplicationComponent//
{
	public $someconfig='somedefault';

	public function init() {
		echo "My component init function";
	}

	public function another_function() {
		return "My component another function";
		
	}
}
